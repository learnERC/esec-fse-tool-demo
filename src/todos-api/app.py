#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os, random
from eve import Eve

mongo_uri = os.environ.get('MONGO_URI')
service_port = 5000

eve_settings = {}
eve_settings['MONGO_URI'] = mongo_uri
eve_settings['MONGO_DBNAME'] = 'todos'
eve_settings['DEBUG'] = True

eve_settings['URL_PREFIX'] = 'api'
eve_settings['API_VERSION'] = 'v1'
eve_settings['RENDERERS'] = ['eve.render.JSONRenderer']
eve_settings['RESOURCE_METHODS'] = ['GET', 'POST']
eve_settings['ITEM_METHODS'] = ['GET', 'PUT', 'PATCH', 'DELETE']
eve_settings['X_DOMAINS'] = '*'
eve_settings['SCHEMA_ENDPOINT'] = 'schema'
eve_settings['DOMAIN'] = {}
eve_settings['DOMAIN']['todos'] = {
    'schema': {
        'title': {
            'type': 'string',
            'required': True,
            'minlength': 1,
            'maxlength': 50,
        },
        'text': {
            'type': 'string',
            'required': True,
            'minlength': 1,
            'maxlength': 250,
        },
    },
}

def pre_get_callback(resource, request, lookup):
  sorted([random.random() for i in range(100000)])

app = Eve(settings=eve_settings)
app.on_pre_GET += pre_get_callback

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=service_port)
