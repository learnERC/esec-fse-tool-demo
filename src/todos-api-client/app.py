#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import signal
import requests
import time
import logging

def handle_sigterm(*args):
  raise KeyboardInterrupt()

def main():
  signal.signal(signal.SIGTERM, handle_sigterm)
  try:
    while True:
      time.sleep(20)
      requests.get('http://todos-api:5000/api/v1/todos')
  except KeyboardInterrupt:
    logging.info('Shutting down...')


if __name__ == '__main__':
  main()
