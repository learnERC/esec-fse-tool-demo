# VARYS - ESEC/FSE 2019 Tool Demo
## Synopsis
This repository contains the requirements, the instructions and the artifacts to reproduce the VARYS tool demo submitted to [ESEC/FSE 2019 Demonstrations Track](https://esec-fse19.ut.ee/calls/tool-demos/). It also holds the links to the repositories with the source code.
A screencast of the demo is available [here](https://drive.google.com/file/d/1lqkzkI-T2FqHtQilValhQ0B_IRrCS-Ta/view).

## Source code repositories
* **maas-client**: [https://gitlab.com/learnERC/varys-maas-client/tree/esec_fse_demo_v2](https://gitlab.com/learnERC/varys-maas-client/tree/esec_fse_demo_v2)
* **maas-server**: [https://gitlab.com/learnERC/varys-maas-server/tree/esec_fse_demo_v2](https://gitlab.com/learnERC/varys-maas-server/tree/esec_fse_demo_v2)
* **monitoring-actuation-bridge**: [https://gitlab.com/learnERC/varys-monitoring-actuation-bridge/tree/esec_fse_demo_v2](https://gitlab.com/learnERC/varys-monitoring-actuation-bridge/tree/esec_fse_demo_v2)

## Requirements
In order to reproduce the demo you have to satisfy the following requirements:
* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/): the Kubernetes CLI
* [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/#install-minikube) or access to a Kubernetes cluster

## Instructions
When your cluster is setup, you have to deploy all the components. Follow the steps below to configure, deploy and repreduce the demo.

### Configure
* Create a cluster role binding to allow you to administrate the cluster (required to if you are using Minikube): `kubectl create clusterrolebinding serviceaccounts-cluster-admin --clusterrole=cluster-admin --group=system:serviceaccounts
`
* Get your default token to access the Kubernetes API (required by the monitoring-actuation-bridge component):
    * Run: `SECRET_NAME=$(kubectl get secrets | grep ^default | cut -f1 -d ' ')`
    * Then, run: `kubectl describe secret $SECRET_NAME | grep -E '^token' | cut -f2 -d':' | tr -d " ")`
* Edit `kubernetes/monitoring-actuation-bridge/000-config-map.yml`:
  * Update the environment variable `KUBERNETES_TOKEN` with your token
  * Check if the default values of the environment variables `KUBERNETES_API` and `KUBERNETES_NAMESPACE` meets your cluster setup (if you run Minikube you shouldn't change them)
    > If you have to switch from the default namespace to another one for any reason, you have to add it within all the YAML files and to change the StatefulSet DNS names to Kafka, Zookeeper and Redis too
* Edit `kubernetes/maas-client/100-deployment.yml` and update the environment variable `VUE_MAAS_SERVER_HOST` to your `minikube ip` or the Kubernetes Node IP
  > The MaaS Server is exposed as NodePort 30000 to allow the MaaS Client to perform client-side AJAX requests

### Deploy

* Run `kubectl apply -f kubernetes/ --recursive` to deploy all the components in one-shot
* Run `minikube dashboard` to open the Kubernetes Dashboard and check the progressing workloads

### Reproduce demo

* Run `kubectl port-forward service/kibana 5601:5601` to access to Kibana at [http://localhost:5601](http://localhost:5601)
* Import the Kibana dashboards provided in the folder `src/kibana-dashboards`:
  * Navigate to [http://localhost:5601/app/kibana#/management](http://localhost:5601/app/kibana#/management)
  * Click on `Saved Objects`, then on `Import` to upload the file `src/kibana-dashboards/export.json`
* Run `kubectl port-forward service/maas-client 8080:8080` to access to the VARYS Web UI (the MaaS Client) at [http://localhost:5601](http://localhost:5601)
* At this point you can perform a monitoring request as shown in the [demo video](https://drive.google.com/file/d/1lqkzkI-T2FqHtQilValhQ0B_IRrCS-Ta/view)
    * You can use the `Performance->Resource consumption` goal and its `subgoals` for any of the Kubernetes Deployments in the cluster
    * On the contrary, you can use `Reliability->Availability->Uptime` only for the MongoDB deployments
* Navigate to [http://localhost:5601/app/kibana#/dashboards](http://localhost:5601/app/kibana#/dashboards) to visualize the Kibana dashboards
  > The dashboards provided visualize data only for the ToDos API application and its MongoDB instance
